var plasmaOn = false;
var laserOn = false;
var laserFiring = false;
var heliumOn = false;
var samplesInCell = false;
var flashingOff = false;
var spotSize = 10;
var repetitionRate = 5;
var selectedElements = []; //array for up to 10 elements to be selected from periodic table
var maxElementsSelected = 10;
var img_ele = null,
	event_start = hasTouch() ? 'touchstart' : 'mousedown',
	event_move = hasTouch() ? 'touchmove' : 'mousemove',
	event_end = hasTouch() ? 'touchend' : 'mouseup';
   //console.log(event_start + "|" + event_move + "|" + event_end);
	
var x_img_ele = 0;
var y_img_ele = 0;

var liveGraph;
var graphInterval;
var errorMessages = data.errors.slice();
var selectedSampleName;
var selectedSampleNum = 1;
var scanListTable;
var sampleTable;
var gasVal=0.00436, torchXVal=0.00436, torchYVal=0.00436; //values when sliders are set to far left hand side
//for converting linear range of sliders on ICP-MS tab to gaussian distribution
var gaussianWidth = 230; //seems to make best use of 0-100 range
var gaussianCentre = 50; //optimal value for slider, using centre at the moment
var zoomLevel = 1;
var scanListSelectedData;
var timeSinceLaserFired = 0;
var timeSinceLaserStopped = 0;
var washout; //phase after laser has fired when signal steadily decays
var graphTime = 0;
var spotCount = 0;
var pitCount = 0;
var laserFiringDuration = 30000; //30 secs for laser ablation
var pauseBetweenAblations = 15000; //time to pause between ablating spots during batch run. ~30 secs in real life but don't want it to get too boring

var overSampleNum = 1;
var liveImageSrc = "";//"images/nist_srm_612.png"; //path to the image being shown in live view
var liveImageName;
var currentSampleId = 0;
var offscreenImageNum;
var batchCounter;
var batchRunning;
var storage = window.localStorage;
var material = "sample";
var laserMode;// = 'sampleHolder';
var sampleHolderLastX;
var sampleHolderLastY;
var sampleHolderLastZoom = 1;
var scaleBarWidth = 20;
var scaleBarUnits = "mm";

var dataOutputSaved = true;
var machineTuned = false;
var tuning; //true during tuning process
var firingOverExistingPit = false;
var warningGiven = false; //flag if the user has been warned about ablating on an existing pit.
var clearingAll = false; //flag set when clearAll btn clicked, otherwise settings are stored again as page reloads!
var slideNum = 0; //keep track of image number during mini slideshows in teaching material
var numberOfSlides;
var slidedata = [];
var removingDisc; //flag
var loadingNewDisc = true;
/*
-----------------------------------------initialisation of tabs and event handlers---------------------------------------------------
*/
$( document ).ready(function() {
	
	//set up main tabs and sub-tabs
    var tabs_main = $( "#tabs_main" ).tabs();
	var tabs_3_instrument = $( "#tabs-3instrument" ).tabs();
	var tabs_3_data = $( "#tabs-3data" ).tabs();
	var tabs_laser = $( "#tabs-12" ).tabs();
	
    tabs_main.find( ".ui-tabs-nav" ).sortable({
      axis: "x",
      stop: function() {
        tabs_main.tabs( "refresh" );
      }
    });
	
	tabs_3_instrument.find( ".ui-tabs-nav" ).sortable({
      axis: "x",
      stop: function() {
        tabs_3_instrument.tabs( "refresh" );
      }
    });
	
	tabs_3_data.find( ".ui-tabs-nav" ).sortable({
      axis: "x",
      stop: function() {
        tabs_3_data.tabs( "refresh" );
      }
    });
	
	tabs_laser.find( ".ui-tabs-nav" ).sortable({
      axis: "y",
      stop: function() {
        tabs_laser.tabs( "refresh" );
      }
    });
	
	//sub-pages for learn about tab
	$(function() {
		$("#pagination_learn").pagination({
			pages: 12,		
			cssStyle: 'light-theme',
			onPageClick: function(pageNumber) { pageClickLearn (pageNumber) }
		});
	});
	
	function pageClickLearn(pageNum)
	{
		for (var i = 1; i <=  $("#pagination_learn").pagination('getPagesCount'); i++)
		{
			$("#learn_page"+i).hide();
		}
		$("#learn_page"+pageNum).show();
		
		if (pageNum == 7)
			createSlideshow(1,6,data.slideshow1data)
		if (pageNum == 12)
			createSlideshow(2,5,data.slideshow2data)
	}
	
	//sub-pages for getting started tab
	$(function() {
		$("#pagination_gettingstarted").pagination({
			pages: 18,		
			cssStyle: 'light-theme',
			onPageClick: function(pageNumber) { pageClickGS (pageNumber) }
		});
	});
	
	function pageClickGS(pageNum)
	{
		for (var i = 1; i <=  $("#pagination_gettingstarted").pagination('getPagesCount'); i++)
		{
			$("#gs_page"+i).hide();
		}
		$("#gs_page"+pageNum).show();
	}

	
	//sliders on ICP-MS tab
	$( "#slider_torch1" ).slider({
	  value: 0,
	  disabled:true, //start with them disabled and only enable during tuning?
      slide: function( event, ui ) {
		sliderTorchX(ui.value);  
      }
    });
	$( "#slider_torch2" ).slider({
	  value: 0,
	  disabled:true,
      slide: function( event, ui ) {
        sliderTorchY(ui.value);
      }
    });
	$( "#slider_gas" ).slider({
	  value: 0,
	  disabled:true,
      slide: function( event, ui ) {
        sliderGas(ui.value);
      }
    });
	
	//drop-downs for changing spot size and repetition rate
	$( "#selectSpotSize" ).selectmenu({
      change: function( event, data ) {
		setSpotSize(data.item.value/5);
		}
	})
	
	$( "#selectRepetitionRate" ).selectmenu({
      change: function( event, data ) {
		setRepetitionRate(data.item.value);
		}
	})

	//buttons for zooming live image
	document.getElementById('btnZoomOut').addEventListener('click', function() {
		$("#btnZoomIn").css('opacity',1);
		zoomLiveImage(null,0.5);
	});
	document.getElementById('btnZoomIn').addEventListener('click', function() {
		$("#btnZoomOut").css('opacity',1);
		zoomLiveImage(null,2);
	});
	
	document.getElementById('btnGotoSelectedSample').addEventListener('click', function() {
	  console.log(scanListSelectedData);
	  var x = scanListSelectedData[4];
	  var y = scanListSelectedData[5];
	  var z = scanListSelectedData[6];
	  var s = scanListSelectedData[7];
	  var id = scanListSelectedData[8];
	  gotoSampleHolderPosition (x,y,z,s,id);
	});
	
	document.getElementById('laserImageDiv').addEventListener(event_start, start_drag);
	document.getElementById('crossHairCircle').addEventListener(event_start, start_drag); //dragging should start if you click within this circle overlay
	
	document.getElementById('laserImageDiv').addEventListener(event_move, while_drag);
	document.getElementById('laserImageDiv').addEventListener(event_end, stop_drag);

	//making sure dragging stops when you mouse out of live image area. maybe just put on body?
	
	document.getElementById('tabs-12').addEventListener(event_end, stop_drag);
	document.getElementById('tabs-21').addEventListener(event_end, stop_drag);
	document.getElementById('tabs-22').addEventListener(event_end, stop_drag);
	document.getElementById('tabs-23').addEventListener(event_end, stop_drag);
	
	$( "input" ).checkboxradio(); //for radio buttons toggling live image/ sample holder
	$( "#moreInfoDialog" ).hide();
	
	$( "#btnFireLaser" ).addClass('ui-state-disabled');
	
	//live image for detailed view
	$('#radio-1').click( function () { 
		enterLiveView (false,overSampleNum)
	} ); 
		
	//sample holder for swapping between up to 6 discs	
	$('#radio-2').click( function () { 
		$("#liveImageDiv").hide();
		$("#sampleHolderLaser").show();
		$("#sampleHolderLaser").css('opacity',1);
		$("#sampleHolderLaser").css('width','320px').css('height','320px'); //shouldn't be necessary but sometimes seems to be zero!	
		$("#sampleHolderLaser").css('cursor','default');
		for (var i=1; i<=6; i++)
		{
			$("#discHolder"+i).css('cursor','pointer');
		}
		//go back to last sample holder pos (or do I need to update if you've repositioned in detailed live view?
		laserMode = 'sampleHolder';
		if (sampleHolderLastZoom > 1)
		{
			zoomLiveImage(sampleHolderLastZoom,null);
		}
		
		setScale();
		
		$("#crossHairCircle").css('opacity',0.01); //hide circle spot. still needs to be just about visible for collision detection with other divs
		$("#crossHairVert").css('opacity',0); 
		$("#crossHairHoriz").css('opacity',0);
		$("#btnZoomIn").css('opacity',0);
		$("#btnZoomOut").css('opacity',0);
		$("#btnPlaceSpot" ).addClass('ui-state-disabled');
		$("#btnFireLaser" ).addClass('ui-state-disabled');
	} ); 
	
	$("#checkboxLaserOn").click( function () {
		if (!this.checked) {
			stopLaser();
		}
		else
			$("#btnFireLaser").removeClass('ui-state-disabled');
	});
		
	//create the 3 data tables and give a tabindex
	sampleTable = $('#sampleTable').DataTable({
		"fnCreatedRow": function( nRow ) {
			$(nRow).attr("tabindex", "0");
			$(nRow).attr("aria-selected", "false");
		}
	});
	scanListTable = $('#scanListTable').DataTable({
		"fnCreatedRow": function( nRow ) {
			$(nRow).attr("tabindex", "0");
			$(nRow).attr("aria-selected", "false");
		}
	});
	
	populateSampleTable();
	
	//common formatting for export of data table could be added here. At present it's just returning the data straight.
	var buttonCommon = {
        exportOptions: {
            format: {
                body: function ( data, row, column, node ) {			
					return data;
                }
            }
        }
    };
	
	//add export buttons - copy and export as excel. Could have PDF too though that's require another JS library file
	//see https://datatables.net/extensions/buttons/
	dataOutputTable = $('#dataOutputTable').DataTable({
		"fnCreatedRow": function( nRow ) {
			$(nRow).attr("tabindex", "0");
			$(nRow).attr("aria-selected", "false");
		},
		dom: 'Bfrtip',
		buttons: [
		
			$.extend( true, {}, buttonCommon, {
                extend: 'excelHtml5',
				action: function ( e, dt, node, config ) {
                // Do custom processing
                dataOutputSaved = true;
 
                // Call the default csvHtml5 action method to create the Excel file (it would do this automatically if I wasn't doing custom actions too
                $.fn.dataTable.ext.buttons.excelHtml5.action.call(this, e, dt, node, config);
            }
				
            } ),
            $.extend( true, {}, buttonCommon, {
                extend: 'copyHtml5',
				text: 'Copy',
				action: function (e, dt, node, config ) {
					dataOutputSaved = true;
					
					// Call the default copy action method to copy data
					$.fn.dataTable.ext.buttons.copyHtml5.action.call(this, e, dt, node, config);
				}		
            } )	
        ]
	});
	
	
	$('#sampleTable').on('keyup', 'tr', function (event) {
		var key = event.which;
		console.log (key)
		//allow keyboard selection of a sample row
		if(key == 13)  // the enter key code
		{
			selectRowInSampleTable(this);		 
		}
	});

    $('#sampleTable tbody').on('click', 'tr', function () {
		selectRowInSampleTable(this);	
    } );
	
		
	$('#dataOutputTable').on('keyup', 'tr', function (event) {
		var key = event.which;
		//allow keyboard selection of a sample row (some duplication of mouse click. refactor)
		if(key == 13)  // the enter key code
		{
			var target = $(event.target);
	   
			if ( $(this).hasClass('selected') ) 
			{ 
				$(this).removeClass('selected'); 
			} 
			else { 
				dataOutputTable.$('tr.selected').removeClass('selected'); 
				$(this).addClass('selected'); 
			} 	 
		}
	});
	
	$('#deleteSelectedRow').click( function () { 
		scanListTable.row('.selected').remove().draw( false ); 
		storage.removeItem(("scanList")+scanListSelectedData[0]);
		calculateBatchRunTime ();
		//remove corresponding marker circle
		var spotToRemove  = "#spot" + scanListSelectedData[0];
		$(spotToRemove).remove();
		} ); 
		
	
	$('#scanListTable').on('keyup', 'tr', function (event) {
		var key = event.which;
		//allow keyboard selection of a sample row (some duplication of mouse click. refactor)
		if(key == 13)  // the enter key code
		{
			var target = $(event.target);
	   
			if ( $(this).hasClass('selected') ) 
		{ 
			$(this).removeClass('selected'); 
		} 
		else { 
			scanListTable.$('tr.selected').removeClass('selected'); 
			$(this).addClass('selected'); 
		} 
		scanListSelectedData = scanListTable.row( this ).data();		 
		}
	});
	$('#scanListTable tbody').on( 'click', 'tr', function () 
	{ 
		//possibly implement ability to edit sample names here. Editor plugin for jquery datatables looks good but costs
		if ( $(this).hasClass('selected') ) 
		{ 
			$(this).removeClass('selected'); 
		} 
		else { 
			scanListTable.$('tr.selected').removeClass('selected'); 
			$(this).addClass('selected'); 
		} 
		scanListSelectedData = scanListTable.row( this ).data();
		
	} );
	
	$("#errorList").html(errorMessages);
	
	for (var i = 0; i<6; i++)
	{
		$("#sampleHolder"+i).keydown(function(event)
			{	
				//console.log (event);
				if (event.keyCode == 13)
				{
					var idNum = event.target.id.substr(event.target.id.length - 1);
					console.log (idNum);
					loadSample(idNum);
				}
			}
		)
	}
	
	for (var i = 1; i<=6; i++)
	{
		$("#discHolder"+i).click(function()
			{	
				laserHolderSampleSelect (this);
			}
		)
		
		$("#discHolder"+i).keydown(function(event)
			{		
				if (event.keyCode == 13)
				{
					laserHolderSampleSelect (this);
				}
			}
		)
	}
	
	//make sample discs draggable into holder as well as by clicking on holder
	$( "#imgSampleDisc" ).draggable(
		{ 
			snap: ".sampleHolder" , //will snap to any of the 6 circular slots in the sample holder
			snapMode: "inner",
		
			stop: function() //called on drop
			{
				//work out which holder position you're over
				var holdernum = 0;
				if(isOverlap("#imgSampleDisc","#sampleHolder1")) holdernum = 1;
				if(isOverlap("#imgSampleDisc","#sampleHolder2")) holdernum = 2;
				if(isOverlap("#imgSampleDisc","#sampleHolder3")) holdernum = 3;
				if(isOverlap("#imgSampleDisc","#sampleHolder4")) holdernum = 4;
				if(isOverlap("#imgSampleDisc","#sampleHolder5")) holdernum = 5;
				if(isOverlap("#imgSampleDisc","#sampleHolder6")) holdernum = 6;
				
				//set the src of the target area you've dropped on to the src of the drag object	
				loadSample(holdernum);
				
				//replace drag object, with src emptied, back where it was
				$("#imgSampleDisc").attr('src', 'images/sample_disc_empty_holder.png');
				$("#imgSampleDisc").css('left','20px');
				$("#imgSampleDisc").css('top','300px');
				holdernum = 0;
			},
			opacity: 1, 	
		}
	);
	
	//radio buttons in p10 of learn about section for toggling between photos of element maps
	$('input[type=radio][name=radio-map1]').change(function() {
		if (this.value == '1') {
			$("#volcanic_glass").attr("src","images/learn/volcanic_glass.jpg");
		}
		else if (this.value == '2') {
			$("#volcanic_glass").attr("src","images/learn/mgo_map.jpg");
		}
		else if (this.value == '3') {
			$("#volcanic_glass").attr("src","images/learn/sc_map.jpg");
		}
	});
	
	// alternative to window unload as an event to trigger storing of settings on iPad
	// subscribe to visibility change events, see https://www.igvita.com/2015/11/20/dont-lose-user-and-app-state-use-page-visibility/
	document.addEventListener('visibilitychange', function() {
	  // fires when user switches tabs, apps, goes to homescreen, etc.
		if (document.visibilityState == 'hidden') { 
				storeSettings();
		}
	});
	
	//shouldn't be necessary in addition to above but just in case...
	$("document").on("pagehide",function(event){
		storeSettings();
	});

	$('#radio-map1').click();
	//retrieve stored values from local storage at end of document load
	retrieveSettings();
});//end document ready

/* 
attempt to store settings when leaving the page. Is this reliable? Think it's deprecated (and doesn't work in Jquery 3+ or in Safari on ipad). Was calling storeSettings any time a control was changed but although this worked locally it didn't set the checkboxes correctly when run from the server - the act of setting one checkbox state in retrieveSettings called storeSettings before the rest had been set.

also, if it's going on OSL and not on students.open.ac.uk then could use VLE storage rather than local storage, which would be better for use across devices
*/

//could be unnecessary if visibility change event above works properly
$( window ).unload(function() {
	  storeSettings();
});


/*
-----------------------------------------ICPMS tab---------------------------------------------------
*/
//checkbox for turning plasma on and off
function plasmaCbHandler (cb)
{
	plasmaOn = cb.checked;
	
	displayError(5,!plasmaOn);
	
	if (!plasmaOn) //stop signal monitor if plasma is turned off
		graphTimer(false);
}

/*sliders to alter flow of carrier gas and xy positions of torch. These each have a nominal optimum position (currently in the centre)
and if you move away from this the signal will be affected, via a gaussian distribution*/
function sliderGas (val)
{
	gasVal = gauss(val, gaussianCentre, gaussianWidth);
}
function sliderTorchX (val)
{
	torchXVal = gauss(val, gaussianCentre, gaussianWidth);
}
function sliderTorchY (val)
{
	torchYVal = gauss(val, gaussianCentre, gaussianWidth);
}

//convert the linear range given by sliders above to a normal distribution, so if you're way off then you'll get
//almost no signal
function gauss(input, centre, width) {
  exptop = 0 - Math.pow(input - centre, 2)
  expbot = width * 2
  return Math.pow(Math.E, exptop/expbot)
}

//this is the process of tuning the ICPMS by tweaking the torch sliders and carrier gas in order to produce the best signal
function tuneICPMS ()
{
	if (!samplesInCell)
	{
		alert ("No samples in the sample cell.");
		return;
	}
	
	if (!heliumOn)
	{
		alert ("Helium is off.");
		return;
	}
	
	if 	(!laserOn)
	{
		alert ("Laser is off.");
		return;
	}
	if (!plasmaOn)
	{
		//Nov 18 - we're now going to allow users to continue and ablate even if plasma is off, they just won't see the signal
		//alert ("Plasma is off."); 
		//return;
	}
	if (selectedElements.length == 0)
	{
		alert ("Select at least one element first so you can see see the change in its signal intensity during tuning");
		return
	}
	
	graphTimer(false); //stop any existing timer (if you were running a batch, say)
	//use NIST SRM 612 for tuning
	enterLiveView(false,1);
	$("#liveImageSampleName").html ("NIST SRM 612");
	currentSampleId = 0; //assumes standard is 1st in data file
	selectedSampleNum = 1;
	
	/* play video here of line being ablated in standard, unless user has turned off the flashing effects */
	$("#tuningVideoDiv").show();
	
	if (!flashingOff)
	{
		$("#tuningVideo").get(0).play();
	}
	
	//re-enable sliders for tuning torch and carrier gas
	$("#slider_gas").slider({ disabled: false });
	$("#slider_torch1").slider({ disabled: false });
	$("#slider_torch2").slider({ disabled: false });
	
	$("#container_ptable").hide(); //hide periodic table so you don't have to tab through all the elements in the background
	
	tuning = true;
	graphTimer(true);
	
	//show standard in live view. don't have a proper image of this at the moment
	//$('#radio-1').click();
	$("#discHolder1").click(); //ensure it's the standard we're looking at
	$('#radio-2').checkboxradio( "disable" );
	
	$("#crossHairCircle").css('top','30px');// start with laser near top
	animateLaser (155,284,120000); //and move it slowly down the screen
		
	//show 'Finish tuning' button, which will lock everything in
	$("#btntuneICPMS").hide();
	$("#btnFinishTuning").show();
	
	//show signal monitor (and disable other tabs?)
	$( "#tabs-3data" ).tabs( "option", "active", 1 );
	$( "#tabs-3data" ).tabs( "disable", 0 );
	$( "#tabs-3data" ).tabs( "disable", 2 );
	
	//disable other controls on lhs?
	$( "#tabs-3instrument").tabs( "disable", 0 );
}

//animate laser to draw line across live image during tuning
function animateLaser(x,y,t)
{
	$("#crossHairCircle").animate({
            left: x+'px',
			top:y+'px'
         },
         {
            duration: t,
        
			start: function() {
			   console.log ('animation start');
			   laserFiring = true;
			   fireLaser();
			   $("#crossHairVert").css('opacity',0); 
			   $("#crossHairHoriz").css('opacity',0);
			   
			   var im = document.getElementById('liveImageDiv');
			   var line = "<div id = 'ablationLine' class = 'ablationLine'></div";			
				$( "#liveImageDiv" ).append( line);
				
				$("#ablationLine").css('left', parseInt($("#crossHairCircle").css('left'))+4 + 'px');
				$("#ablationLine").css('top', $("#crossHairCircle").css('top'))
				
            },
            complete: function() {
				console.log ('ablation of line complete');
				$("#crossHairCircle").css('top','30px');
				$("#crossHairCircle").css('left','155px');
				//if it gets to end, animate another line. Currently just repeats the same path but could shift. Not sure many people are going to be looking at this as you need to be using sliders on ICP-Ms tab so not worth making it too fancy!
				animateLaser(155,284,120000)
				
            },
            progress: function(animation, progress) {
               //console.log ('animation progress');
			   
			   //draw an ablation line as the laser moves
			var lineHeight = parseInt($("#crossHairCircle").css('top')) - 28;
			   $("#ablationLine").css('height', lineHeight+'px');
			
              }
		}
      );
}

//when you've finished tuning, check if reasonable settings have been achieved
function finishTuning ()
{
	$( "#crossHairCircle" ).stop(); //stop the animating laser
	$('#radio-2').checkboxradio( "enable" );
	//check tuning sliders are reasonably sensibly positioned
	if (torchXVal <0.6 || torchYVal <0.6 || gasVal <0.6)
	{
		alert ("It doesn't look like you've tuned the ICPMS correctly. Try adjusting the sliders again to achieve a higher signal intensity");
		//return; //without this return it allows users to get out of tuning without achieving correct tuning
	}
	else
	{
		displayError(6, false); //hide error, OK to continue
		machineTuned = true;
		alert ("ICPMS tuned.");
	}
	$("#tuningVideo").get(0).pause();
	$("#tuningVideoDiv").hide();
	
	$("#container_ptable").show();
	
	//lock sliders. 
	$("#slider_gas").slider({ disabled: true });
	$("#slider_torch1").slider({ disabled: true });
	$("#slider_torch2").slider({ disabled: true });
	
	//re-enable disabled tabs
	$( "#tabs-3data" ).tabs( "enable", 0 );
	$( "#tabs-3data" ).tabs( "enable", 2 );
	$( "#tabs-3instrument").tabs( "enable", 0 );
	
	
	$("#btntuneICPMS").show();
	$("#btnFinishTuning").hide();
	
	//to do: re-centre the cross hairs on the live image
	$("#crossHairVert").css('opacity',1); 
	$("#crossHairHoriz").css('opacity',1);
	$("#crossHairCircle").css('left','155px').css('top','155px')
	stopLaser();
	
	$("#btnSignalMonitorReset").click(); //stop signal monitor automatically, or do we want to get users in the habit of doing this?
	
	//put in sample holder mode on laser tab
	$('#radio-2').click();
	/*
	$('#radio-1')[0].checked=false; 
    $('#radio-2')[0].checked=true; 
    $('#radio-1').button("refresh");
	$('#radio-2').button("refresh");
	*/
	tuning = false;
}

/*periodic table*/
function autoSelect(element_id) {

	var elem = document.getElementById(element_id);
	
	if (elem) {
		elem.select();
	}
}

//display info about the selected element
function showLargeElement(atomic_number) {

	var all_elements = document.getElementById('element_large_container').childNodes;
	
	// hide all existing elements
	for (i = 0; i < all_elements.length; i++) {
	
		if (all_elements[i].id != null) {
			var elemID = all_elements[i].id;	
			document.getElementById(elemID).style.display = 'none';
		}
	}
	
	// show the one we want
	if (atomic_number != null)
	{
		document.getElementById('element_id_' + atomic_number).style.display = 'block';
		document.getElementById('element_large_container').style.display = 'block';
		$('#element_num_' + atomic_number).addClass('elementHovered');
	}
	else
	{	
		document.getElementById('element_large_container').style.display = 'none';
	}
	
}

function showDefaultElement() {
	showLargeElement(1);
}

//toggle selection of elements on periodic table by clicking on them and add to array of selected elements
function selectElement (atomic_number)
{
	if (selectedElements.length == maxElementsSelected && $( "#element_num_" + atomic_number ).hasClass( "elementSelected" )==false)
	{
		alert("No more than 10 elements can be selected at once. Deselect one you've already chosen if you want to select this element.");
		return;
	}
	
	$('#element_num_' + atomic_number).toggleClass('elementSelected');
	showLargeElement(atomic_number);
	
	if ($( "#element_num_" + atomic_number ).hasClass( "elementSelected" ))
	{
		selectedElements.push(atomic_number);
	}
	else
	{
		selectedElements.splice( $.inArray(atomic_number, selectedElements), 1 );
		//to do: delete that element column from data output table?
	}
	graphTimer(false);
	selectElements();

}

/* lock in the choices made on the periodic table and add series to the live graph and headings to data output table */
function selectElements ()
{
	graphTimer(false);
	plotCountGraph();
	displayError(7, false);
	for (var i = 0; i < 10; i++)
	{
		$("#th"+(i+1)).html(""); //clear out old headings before re-populating
	}
	
	for (var i = 0; i < selectedElements.length; i++) {
		var atomicNum = parseInt(selectedElements[i]);
		var symbol = chemicalElements.elements[atomicNum-1].symbol;
		var units = " (ppm)";
		//add oxides for major elements
		if (symbol == "Ca") { symbol = "CaO"; units = " (wt%)"; }
		if (symbol == "Mg") { symbol = "MgO"; units = " (wt%)"; }
		if (symbol == "Al") { symbol = "Al<sub>2</sub>O<sub>3</sub>"; units = " (wt%)"; }
		if (symbol == "K") { symbol = "K<sub>2</sub>O"; units = " (wt%)"; }
		if (symbol == "Na") { symbol = "Na<sub>2</sub>O"; units = " (wt%)"; }
		if (symbol == "Ti") { symbol = "TiO<sub>2</sub>"; units = " (wt%)"; }
		if (symbol == "Fe") { symbol = "Fe<sub>2</sub>O<sub>3</sub>"; units = " (wt%)"; }
		if (symbol == "Si") { symbol = "SiO<sub>2</sub>"; units = " (wt%)"; }
		 
		$("#th"+(i+1)).html(symbol + units);
			
		
	}
	if (selectedElements.length == 0)
		displayError(7, true);
	
	
	//clear data table otherwise titles change but data stays so columns are wrong
	//To do: work out how to remove just one column and shift everything along?
	for (var i=0;i<dataOutputTable.rows().count(); i++)
	{
		dataOutputTable.$('tr').eq(i).addClass('toRemove'); //flag these rows for removal. trying to remove directly doesn't seem to work 
	}
		
	dataOutputTable.rows( '.toRemove' ).remove().draw(); //now we can remove all the rows 
		
}

function elementMouseOut()
{
	for (i = 1; i <= 118; i++) {
		$('#element_num_' + i).removeClass('elementHovered');
	}
}


/*
-----------------------------------------laser tab---------------------------------------------------
*/

//called when one of the discs in the sample holder is clicked on or activated by pressing enter. Just puts that sample into live view
function laserHolderSampleSelect (t)
{
	if (laserMode == 'live') return;
	var im = t.src.substr(0, t.src.indexOf('_holder'))+'.png';
	
	liveImageSrc = im;
	liveImageName = $(t).attr('data-sample-name');
	currentSampleId = $(t).attr('data-sample-id');
//console.log ("currentSampleId", currentSampleId);	
	//get the holder num from the image id;
	overSampleNum = t.id[t.id.length -1];

	enterLiveView(false, overSampleNum);
}

//called on entering live view	
function loadLiveImage (im)
{
	console.log (im);
	$("#liveImage").attr('src', im);
	/*for each live image we need a 'masked' version that is coloured so we can detect through cursor position
	whether it's over sample, epoxy (coloured red) or existing pit (black) */
	var pos = im.indexOf(".");
	//all offscreen versions of image need to be named as main live image but with _offscreen appended
	var offscreenIm = [im.slice(0, pos), '_offscreen', im.slice(pos)].join('');
	$("#liveImageOffscreen").attr('src', offscreenIm);
	
	$("#liveImageDiv").show();	
}

/* 
show the detailed live view (reflected light image) of the selected sample disc
this can be called in run batch mode too, in which case need to pass the sample as parameter from the scan list
*/
function enterLiveView (navigatingFromScanList, sampleNum)
{
	var sample;
//console.log ("enter live view", sampleNum)	
	//hide line ablated during tuning (unless we're on standard?)
	if (sampleNum>0) 
		$("#ablationLine").hide();

	$("#btnFireLaser" ).removeClass('ui-state-disabled');

	if (tuning)
	{
		$("#crossHairVert").css('opacity',0); 
		$("#crossHairHoriz").css('opacity',0);
		
	}
	else
	{
		$("#crossHairVert").css('opacity',1); 
		$("#crossHairHoriz").css('opacity',1);
	}	
	
	if (!tuning) //not sure if zoom btns are necessary during tuning. If we do have them then I need to make the ablation line zoom too
	{		
		$("#btnZoomIn").css('opacity',1);
		$("#btnZoomOut").css('opacity',1);
	}
	else
	{		
		$("#btnZoomIn").css('opacity',0);
		$("#btnZoomOut").css('opacity',0);
	}
	
	var sampleIm = $("#discHolder"+sampleNum).attr('src');
	loadLiveImage (sampleIm.substr(0, sampleIm.indexOf('_holder'))+'.png');
	$("#liveImageSampleName").html(liveImageName);
	$("#sampleHolderLaser").hide();//css('opacity',0);
	for (var i=1; i<=6; i++)
	{
		$("#discHolder"+i).css('cursor','move');
	}
	$("#sampleHolderLaser").css('cursor','move');
	$( "#btnFireLaser" ).removeClass('ui-state-disabled');
	zoomLiveImage(1, null);
	laserMode = 'live';
	
	$("#crossHairCircle").css('opacity',1); //show circle spot
	$( "#btnPlaceSpot" ).removeClass('ui-state-disabled');

	//only show spots and pits associated with this sample
	for (var i = 1; i<= spotCount; i++)
	{
		if ($("#spot"+i).hasClass( "sample"+ sampleNum))
		{
			var ss;
			if (navigatingFromScanList)
				ss = parseInt(scanListSelectedData[2])/5;
			else
				ss = spotSize;
			
			$("#spot"+i).css('opacity',1);//.css('width',ss+'px').css('height',ss+'px');
			//console.log ("set spot size", ss, navigatingFromScanList);
		}
		
		else
			$("#spot"+i).css('opacity',0);		
	}
	
	
	for (var i = 1; i<= pitCount; i++)
	{
		if ($("#pit"+i).hasClass( "sample"+ sampleNum))
		{
			$("#pit"+i).css('opacity',1);
		}
		else
		{
			$("#pit"+i).css('opacity',0);
		}
	}
	$('#radio-1')[0].checked=true; //put in live image mode
    $('#radio-2')[0].checked=false; 
    $('#radio-1').button("refresh");
	$('#radio-2').button("refresh");
	$("#liveImageDiv").css('width','320px').css('height','320px'); //shouldn't be necessary but sometimes seems to be zero!	
	setScale ();
}

//check box to turn laser on and off (not the same as firing and stopping)
function laserCbHandler (cb)
{
	laserOn = cb.checked;
	displayError(4,!laserOn);
	if (!laserOn)
		$( "#btnFireLaser" ).addClass('ui-state-disabled');
}

//drop down for repetition rate (how many times the laser fires in a second. this is indicated visually by flashing of the light.
function setRepetitionRate (hz)
{
	repetitionRate = hz;	
}

function setSpotSize (size)
{
	spotSize = size;

	//redraw cross hair circle and scale
	$(".crossHairCircle").css('width',spotSize*zoomLevel+'px').css('height',spotSize*zoomLevel+'px').css('left',160-zoomLevel*spotSize/2+'px').css('top',160-zoomLevel*spotSize/2+'px');
}

//add a spot visually to the live image (yellow cross) and add details of location, sample num etc. to the scan list table ready for batch analysis
function placeSpot ()
{
	var im = document.getElementById('liveImageDiv');

	spotCount++;
	//temporary fudge - set it to the standard if we've not selected an unknown. But may break if we have 2 unknowns?
	if (!liveImageName) 
		liveImageName = "NIST SRM 612";	
	if (!currentSampleId) 
		currentSampleId = "0";	
	
	
	
	//place visual indicator within live image
	var spot = "<div id = 'spot"+spotCount +"' class = 'spotMarker'></div";
	
	var imLeft = parseInt(im.style.left);
	var imTop = parseInt(im.style.top);
	if (isNaN(imLeft)) imLeft = 0;
	if (isNaN(imTop)) imTop = 0;
	var l = 159 - imLeft - zoomLevel*spotSize/2;
	var t = 159 - imTop  - zoomLevel*spotSize/2;
	
	//check if spot is actually on slide and not off the edge  on white border
	var w = parseInt($("#liveImage").css('width'));
	var h = parseInt($("#liveImage").css('height'));
	var r = parseInt($("#crossHairCircle").css('width'))/2; //radius of cross hair circle
	//console.log (l,t, r);
	//console.log (w,h);
	
	if (l+r<0 || l+r>w || t+r<0 || t+r> h) 
	{
		alert ("not on sample disk")
		return;
	}
	sampleOrEpoxy(l,t);

	$( "#liveImageDiv" ).append( spot);
	
	$("#spot"+spotCount).css('left',l+'px').css('top',t+'px');
	$("#spot"+spotCount).css('position','absolute').css('width',spotSize*zoomLevel+'px').css('height',spotSize*zoomLevel+'px');
	
	$("#spot"+spotCount).addClass ('sample'+overSampleNum); //add a class I can use to track which spot is on which sample
console.log ("material spot added to is "  + material)
	addToScanList(liveImageName, spotSize, repetitionRate, im.style.left, im.style.top, zoomLevel, spotCount, overSampleNum, currentSampleId, material);
}

//fire laser for 30 secs 
function fireLaser()
{
	if (!samplesInCell)
	{
		alert ("No samples in the sample cell.");
		return;
	}
	
	if (!heliumOn)
	{
		alert ("Helium is off.");
		return;
	}
	
	
	if (!laserOn)
	{
		displayError (4, true);
		
		alert ("Laser is off (you can't ablate samples).");
		laserFiring = false;
		return;
	}
	else
	{
		
		if (!batchRunning) firingOverExistingPit = false;
		
		// check if over existing ablation pit and not in batch mode?
		if (!tuning)
		{
			for (var i=1; i<=pitCount; i++)
			{
				if (!batchRunning && isOverlap ("#crossHairCircle","#pit"+i) && $("#pit"+i).css('opacity') == 1)
				{
					//alert ("Firing laser over existing ablation pit!");
					firingOverExistingPit = true;
				}
				else
				{
					//console.log ("not over existing pit");
				}
			}				
		}
		//material = "sample";
		if (!tuning)
		{
			var im = document.getElementById('liveImageDiv');
			var imLeft = parseInt(im.style.left);
			var imTop = parseInt(im.style.top);
			if (isNaN(imLeft)) imLeft = 0;
			if (isNaN(imTop)) imTop = 0;
			var l = 159 - imLeft - zoomLevel*spotSize/2;
			var t = 159 - imTop  - zoomLevel*spotSize/2;
			
			//check if spot is actually on slide and not off the edge  on white border
			var w = parseInt($("#liveImage").css('width'));
			var h = parseInt($("#liveImage").css('height'));
			var r = parseInt($("#crossHairCircle").css('width'))/2; //radius of cross hair circle
			console.log (l,t, r);
			
			sampleOrEpoxy(l,t); //check if on glass or epoxy, in the same way as when placing spots
		}
		graphTimer (false); //stop existing timer
		
		timeSinceLaserFired = 0;
		washout = false;
		timeSinceLaserStopped = 0;
		
		laserFiring = true;
		$( "#btnFireLaser").hide();
		$( "#btnStopLaser").show();
		
		//$("#btnSignalMonitorStart").click(); //start signal monitor automatically, or do we want to get users in the habit of doing this?
		graphTimer(true);

		//don't allow users to change spot diameter and rep rate during firing
		$( "#selectSpotSize" ).selectmenu( "disable" );
		$( "#selectRepetitionRate" ).selectmenu( "disable" );
		
		//or to run batch/ place spot
		$( "#btnPlaceSpot" ).addClass('ui-state-disabled');
		$( "#btnRunBatch" ).addClass('ui-state-disabled');
		
		//add a CSS class to flash light at either 5 or 10 hertz. n.b. could this strobe effect cause some people problems so there's a checkbox to turn off this effect in the welcome tab
		$("#crossHairCircle").removeClass('laserLight5');
		$("#crossHairCircle").removeClass('laserLight10');
		$("#crossHairCircle").removeClass('laserLight');
		
		if (!flashingOff)
			$("#crossHairCircle").addClass('laserLight'+repetitionRate); 
		else
			$("#crossHairCircle").addClass('laserLight'); //just a plain no-flashing laser!
		
		if (!tuning)
		{
			setTimeout(function(){ 
				stopLaser();
				$( "#btnFireLaser" ).removeClass('ui-state-disabled');
			}, laserFiringDuration);
		}
	}
}

function stopLaser()
{
	if (!tuning && laserFiring)
	{
		createAblationPit();
		washout = true;
	}
			
	laserFiring = false;
	
	$("#crossHairCircle").removeClass('laserLight10');
	$("#crossHairCircle").removeClass('laserLight5');
	$("#crossHairCircle").removeClass('laserLight');
				
	$( "#selectSpotSize" ).selectmenu( "enable" );
	$( "#selectRepetitionRate" ).selectmenu( "enable" );
	$( "#btnPlaceSpot" ).removeClass('ui-state-disabled');
	$( "#btnRunBatch" ).removeClass('ui-state-disabled');
	
	$( "#btnFireLaser").show();
	$( "#btnStopLaser").hide();
}


//add a dark circle spot to the live image to make it look like a pit has been ablated. Positions are stored and retrieved so over time will get quite cratered
function createAblationPit()
{
	pitCount++;
	
	//place visual indicator within live image
	var im = document.getElementById('liveImageDiv');
	var pit = "<div id = 'pit"+pitCount +"' class = 'ablationPit'></div";
	var imLeft = parseInt(im.style.left);
	var imTop = parseInt(im.style.top);
	if (isNaN(imLeft)) imLeft = 0;
	if (isNaN(imTop)) imTop = 0;
	var l = 159 - imLeft - zoomLevel*spotSize/2;
	var t = 159 - imTop  - zoomLevel*spotSize/2;
	
	$( "#liveImageDiv" ).append( pit);
	$("#pit"+pitCount).css('left',l+'px').css('top',t+'px');
	$("#pit"+pitCount).css('position','absolute').css('width',spotSize*zoomLevel+'px').css('height',spotSize*zoomLevel+'px');
	
	$("#pit"+pitCount).addClass ('sample'+overSampleNum); //add a class I can use to track which pit is on which sample
	
	if (isLocalStorageSupported())
	{
		storage.setItem("pit"+pitCount, (imLeft+','+imTop+','+spotSize+','+zoomLevel + ','+overSampleNum));
		storage.setItem("pitCount",pitCount);
	}
	//console.log ("create pit " + pitCount);
}

//start a batch run of all placed spots. Warn user if data from last batch run not already saved
function buttonRunBatchClick()
{
	if (!dataOutputSaved)
	{
		$("#overwriteDataDialog").dialog({
         title: "Overwrite data?",
		 modal:true,
         buttons: {
             "Cancel": function() {
                 $(this).dialog('close');
				 return;
             },
             "Continue": function() {
                $(this).dialog('close');
				runBatch(true)
             }
         }
     });
	}
	else
		runBatch(true);
}

//loop through all rows in scan list, start a timer that drives the stage and ablates spots automatically
function runBatch(running)
{	
	if (scanListTable.rows().count() == 0)
	{
		alert ("No spots placed.");
		return;
	}
	
	if (!samplesInCell)
	{
		alert ("No samples in the sample cell.");
		return;
	}
	
	if (!heliumOn)
	{
		alert ("Helium is off.");
		return;
	}
	
	if 	(!laserOn)
	{
		alert ("Laser is off.");
		return;
	}
	if (!plasmaOn)
	{
		alert ("Plasma is off.");
		return;
	}
	
	if (!machineTuned)
	{
		alert ("ICPMS is not tuned.");
		return;
	}
	
	/*warn if scan list contains spots that have already been analysed (just looking to see if any rows in scan list have been set to the 'analysed' style */
	var spotsAlreadyAnalysed = false;
	
	var data = scanListTable.rows().data();
	data.each(function (value, index) {	
		if (scanListTable.$('tr').eq(index).hasClass('rowAnalysed'))
			spotsAlreadyAnalysed = true;	
	});

	if (spotsAlreadyAnalysed)
		alert("Your scan list includes spots that have already been analysed. Ablating over an existing ablation pit may cause unexpected results!")
		
	if (!running) //stop the batch
	{
		$("#btnRunBatch").show();
		$("#btnStopBatch").hide();
		batchRunning = false;
		clearInterval (batchInt);
		$("#crossHairCircle").removeClass('laserLight10');
		$("#crossHairCircle").removeClass('laserLight5');
		$("#crossHairCircle").removeClass('laserLight');
		laserFiring = false;
		washout = true;
		$("#btnSignalMonitorReset").click(); //stop signal monitor automatically
		$( "#selectSpotSize" ).selectmenu( "enable" );
		$( "#selectRepetitionRate" ).selectmenu( "enable" );
	}
	else //start the batch
	{
		$("#btnStopBatch").show();
		$("#btnRunBatch").hide();
		graphTimer (false); //stop any existing timer before starting again
		//$("#btnSignalMonitorStart").click(); //start signal monitor automatically
		graphTimer(true);
		
		batchCounter = 0;
		batchStep();
		//maybe need to disable manual controls while it's running a batch	
		//don't allow users to change spot diameter and rep rate during firing
		$( "#selectSpotSize" ).selectmenu( "disable" );
		$( "#selectRepetitionRate" ).selectmenu( "disable" );
		
		batchInt = setInterval(batchTimer, laserFiringDuration+pauseBetweenAblations);
		batchRunning = true;
		
		function batchTimer() 
		{
			//highlight/ mark the row in the scan list as having been analysed
			scanListTable.$('tr').eq(batchCounter-1).addClass('rowAnalysed');//this should be enough to change bg color
			scanListTable.$('tr').eq(batchCounter-1).css('background-color', '#b1ff8b');
						
			if (batchCounter == scanListTable.rows().count()) //all rows in scan list analysed, finish up
			{
				$("#batchCompleteDialog" ).dialog();
				$("#batchCompleteDialog").css('display','block');
				clearInterval (batchInt);
				batchRunning = false;
				$("#btnRunBatch").show();
				$("#btnStopBatch").hide();
				
				addToDataTable(scanListSelectedData[8])
			}
			else
			{
				addToDataTable(scanListSelectedData[8])
				batchStep(); //each step (see below) moves the sample to a new placed spot position	
			}	
		}
	}
}

//individual spots analysed during batch run
function batchStep()
{
	scanListSelectedData = scanListTable.row( batchCounter ).data();
	batchCounter++;

	//get coordinate info from each row of scanList
	var x = scanListSelectedData[4];
	var y = scanListSelectedData[5];
	var z = scanListSelectedData[6];
	var s = scanListSelectedData[7];
	var id = scanListSelectedData[8];

	spotSize = parseInt(scanListSelectedData[2])/5;
	repetitionRate = parseInt(scanListSelectedData[3]);
	
//console.log ("scanListSelectedData = ", scanListSelectedData);	
	gotoSampleHolderPosition (x,y,z,s,id);
}


/*image panning and zooming*/

function hasTouch() {
			return 'ontouchstart' in document.documentElement;
	}

//zoom the live image, either to a specified level or by an increment - doubling or halving
function zoomLiveImage(level, increment) {
	
	target = 'liveImageDiv'; //no longer zooming sample holder
	
	img_ele = document.getElementById(target);

	/* Set the scaling factor */
	//console.log ("increment"+increment);
	
	//constrain amount of zooming in and out
	if (zoomLevel<=1 && increment==0.5) 
	{
		return;
	}
	if (zoomLevel>=4 && increment==2) 
	{
		return;
	}
	
	if (increment!=null)
	{
		zoomincrement = increment; // directly, or...	
		zoomLevel = zoomLevel * increment;
		
	}
	else
	{
		
		zoomincrement = level/zoomLevel; //...relative to current level
		zoomLevel = level;				
	}
	
	var pre_width = img_ele.getBoundingClientRect().width,
		pre_height = img_ele.getBoundingClientRect().height;
	
	/* Scaling the image: Make it wider and taller (or slimmer and shorter) to match the zoom increment */
	img_ele.style.width = (pre_width * zoomincrement) + 'px';
	img_ele.style.height = (pre_height * zoomincrement) + 'px';

	/* Relocating the image:
		e.g. if the crosshair is at (5,10) relative to the top-left corner of the sample holder,
		and we zoom in 4x, we now want it to be at (20, 40) relative to the top-left corner of the sample holder
		-- in other words, we render the scaled image at (-20, -40) relative to the crosshair
	*/
	
	/* Step one: Where is the crosshair? */
	crosshair_x = 161; // at the moment the crosshair is at 161, 161
	crosshair_y = 161; // (relative to the top left of the parent div)
	
	/* Step two: Where is the crosshair relative to the image? --
				 or in other words, where is the image relative to the crosshair? */
	relative_x = crosshair_x - img_ele.offsetLeft;
	relative_y = crosshair_y - img_ele.offsetTop;
	/* e.g. if the top-left corner of the sample holder is under the crosshair,
			relative_x would be 0 and relative_y would be 0 */

	/* Step three: Set the top-left of the scaled image relative to the crosshair.
				   We translate up by relative_x scaled by the zoom level,
				   and left by relative_x scaled by the zoom level */
	new_left = -(relative_x * zoomincrement) + crosshair_x;
	new_top = -(relative_y * zoomincrement) + crosshair_y;

	
	/* Step four: Set the image element to the desired position */
	img_ele.style.left = new_left + "px";
	img_ele.style.top = new_top + "px";

	/* Resizing the crosshair: Since we've zoomed in, the crosshair should also be bigger... */
	$(".crossHairCircle").css('width',spotSize*zoomLevel+'px').css('height',spotSize*zoomLevel+'px').css('left',160-zoomLevel*spotSize/2+'px').css('top',160-zoomLevel*spotSize/2+'px');
	
	
	/* ...as should markers for spots we've already placed */
	//$(".spotMarker").css('width',spotSize*zoomLevel+'px').css('height',spotSize*zoomLevel+'px');
	
	/* Iterate through the spots and scale their positions as appropriate */
	$(".spotMarker").each( 
		function (index) {
			/* double the x and y when zoomincrement is 2, etc. */
			$(this).css('left', parseInt($(this).css('left')) * zoomincrement + 'px');
			$(this).css('top', parseInt($(this).css('top')) * zoomincrement + 'px');	
			$(this).css('width', parseInt($(this).css('width')) * zoomincrement + 'px');	
			$(this).css('height', parseInt($(this).css('height')) * zoomincrement + 'px');				
		}
	)
	
	/* ...and any ablation pits we've added */
	//$(".ablationPit").css('width',spotSize*zoomLevel+'px').css('height',spotSize*zoomLevel+'px');
	
	/* Iterate through the spots and scale their positions as appropriate */
	$(".ablationPit").each( 
		function (index) {
			/* double the x and y when zoomincrement is 2, etc. */
			$(this).css('left', parseInt($(this).css('left')) * zoomincrement + 'px');
			$(this).css('top', parseInt($(this).css('top')) * zoomincrement + 'px');	
			$(this).css('width', parseInt($(this).css('width')) * zoomincrement + 'px');	
			$(this).css('height', parseInt($(this).css('height')) * zoomincrement + 'px');	
		}
	)
	
  	img_ele = null;
	
	//constrain amount of zooming in and out
	if (zoomLevel<=1) 
	{
		$("#btnZoomOut").css('opacity',0.3)
	}
	if (zoomLevel>=4) 
	{
		$("#btnZoomIn").css('opacity',0.3);
	}
	
	setScale ();
}

//animate the sample holder to a give coordinate and zoom level for a particular sample
function gotoSampleHolderPosition (x,y,z,sample, id)
{
//console.log ("go to ", x, y, z,sample, id);

	imgEl = document.getElementById('sampleHolder');
	currentSampleId = id;
	//load up the correct sample
	enterLiveView (true,sample)
	zoomLiveImage(z);

	//animate into position
	if (x != null && y != null)
	{
		$("#liveImageDiv").animate({
            left: x,
			top:y
         },
         {
            duration: 3000,
        
			start: function() {
               //console.log ('animation start');
            },
            complete: function() {
				//console.log ('animation complete');
				
				//check if crosshairs over any existing ablation pit
				warningGiven = false;
				for (var i = 1; i<=pitCount; i++)
				{
			
					//if we're about to ablate on a spot where there's already an ablation pit
					if (isOverlap ("#crossHairCircle","#pit"+i) && $("#pit"+i).css('opacity') == 1)
					{
						//flag so we can reduce signal intensity
						firingOverExistingPit = true;
						console.log ("Ablating over an existing ablation pit! ");
					}
					
					if (!warningGiven && firingOverExistingPit && batchRunning)
					{
						//to do: stop this being a modal alert dialog as this stops the whole batch running? Better to check at start of batch if this has already been run
						//alert ("Ablating over an existing ablation pit! "); 
						
						warningGiven = true;
					}
				}
	
				laserFiring = true;
				if (batchRunning && laserOn)
					fireLaser();
            },
            progress: function(animation, progress) {
               //console.log ('animation progress');
            }
		}
      );	
	}
}

//dragging the sample around in laser mode
function start_drag(event) {
	console.log ("start drag")
	if (laserFiring) return; //disable stage movement while laser dragging?
	
	var x_cursor = hasTouch() ? event.changedTouches[0].clientX : event.clientX,
			y_cursor = hasTouch() ? event.changedTouches[0].clientY : event.clientY;
	
	//in this case we always want it to be the live image that's dragged, even if you click to start dragging in the central circle spot overlay. 
	//10-10-2018 Now only allowing dragging of live image
	
	target = 'liveImageDiv';
	
	img_ele = document.getElementById(target) //this;		
	x_img_ele = x_cursor - img_ele.offsetLeft;
	y_img_ele = y_cursor - img_ele.offsetTop;
	
	//turn off default scrolling when dragging the live image, otherwise vertical panning of image results in vert scrolling of page
	//nb also turns off pinch zoom but this isn't implemented for zooming on image anyway (need to use +/- buttons
	event.preventDefault(); 
	
}

function stop_drag() {
	img_ele = null;	
	//console.log ("stop drag")
	$(document).off('touchstart'); //turn on default scrolling again
}

function while_drag(event) {
	//console.log ("dragging "+img_ele)
	//console.log(event.buttons)
	var x_cursor = hasTouch() ? event.changedTouches[0].clientX : event.clientX,
			y_cursor = hasTouch() ? event.changedTouches[0].clientY : event.clientY;
	//ANS added Jan 2020 to check if mouse is down when dragging (0 means no). check this still works on iPad, IE etc.
	if (img_ele !== null && event.buttons !=0) {
		img_ele.style.left = (x_cursor - x_img_ele) + 'px';
		img_ele.style.top = (y_cursor - y_img_ele) + 'px';	
	}
}	

//deal with arrow keys for panning image
function dragImageKeyboardHandler (e)
{
	target = '#liveImageDiv';
	var l = parseInt($(target).css('left'));
	var t = parseInt($(target).css('top'));
	if (e.shiftKey) 
		increment = 50;
	else
		increment = 5;
		
	switch (e.keyCode)
	{
    case 37:
        $(target).css('left', l+increment+'px');
        break;
    case 38:
        $(target).css('top', t+increment+'px');
        break;
	
	case 39:
        $(target).css('left', l-increment+'px');
        break;
	
	case 40:
        $(target).css('top', t-increment+'px');
        break;
	
	case 65: //a to zoom in
        zoomLiveImage(null,2);
        break;
	
	case 90: //z to zoom out
        zoomLiveImage(null,0.5);
        break;
	}
}

//update number associated with scale bar. scalebar itself stays the same size
function setScale ()
{
	if (laserMode == "sampleHolder")
	{
		scaleBarUnits = " mm";
		scaleBarWidth = 40 / zoomLevel;
	}
	else //live image, scale bar width calibrated against size of pits in real photos
	{
		scaleBarUnits = " µm";
		scaleBarWidth = 200 / zoomLevel;
	}

	$("#scaleBarText").html (scaleBarWidth + scaleBarUnits);
}

/*
-----------------------------------------sample change tab---------------------------------------------------
*/

//read from data file into sample table
function populateSampleTable ()
{
	//console.log(data.samples);
	
	for (var i = 0; i<data.samples.length; i++)
	{
		var tectonicSetting = data.samples[i].tectonic_setting;
		var sampleName = data.samples[i].title;
		var sampleSrc = data.samples[i].imageSource;
		var sampleIdNum = data.samples[i].idNum;
		var sampleLocation = data.samples[i].location;
			
		/*add row. n.b only items that have an associated table header will be displayed but the rest are still stored
		as associated data */
		sampleTable.row.add( [
				tectonicSetting,
				sampleName,
				sampleSrc,
				sampleIdNum,
				sampleLocation
			] ).draw( false );
	}	
}

//when a row in the sample table is clicked, just places the selected sample in the circle to the left of the holder, ready to drag into one of the 6 slots.
//passes some data to this sample as custom attributes
function selectRowInSampleTable(t)
{
	if ( $(t).hasClass('selected') ) 
	{ 
		$(t).removeClass('selected'); 
	} 
	else { 
		sampleTable.$('tr.selected').removeClass('selected'); 
		$(t).addClass('selected'); 
	} 
	var data = sampleTable.row( t ).data();
	//console.log ("data stored in sample table row = ", data);
	var selectedSampleSrc = data[2];
		
	$("#imgSampleDisc").attr('src', 'images/'+selectedSampleSrc+'_holder.png');	
	$("#imgSampleDisc").attr('data-sample-name', data[1]);	
	$("#imgSampleDisc").attr('data-sample-id', data[3]); //store the id num from the data file		
}
	
//called by clicking on a slot in the sample holder to load a sample disc, or by dragging and dropping into a slot
function loadSample (num)
{
	//don't allow change of sample disc if holder is in sample cell and/or helium is on
	if (heliumOn || samplesInCell)
	{
		alert ("You need to turn the helium off and unload the holder from the sample cell before you can do a sample change.")
		return;
	}
	
	selectedSample_disc = $("#imgSampleDisc").attr('src'); //the image that's to the left of the sample holder
	var name  = $("#imgSampleDisc").attr('data-sample-name');
	
	var id = $("#imgSampleDisc").attr('data-sample-id');
//console.log ("ID: ",id);	
	//check if sample is already in holder and don't allow it to be put in twice!
	for (var i = 1; i<6; i++)	
	{	
		var sampleInHolder = getSampleNameInPosition (i); //actually this is the source not the name
		if (selectedSample_disc.indexOf(sampleInHolder)>0 && selectedSample_disc.indexOf('empty')<0)
		{
			alert ("sample already in holder");
			return;
		}	
	}
	
	

	//check if the 2 standards are being loaded and turn off errors if so
	if (id == '0')
	{
		displayError(0,false); //NIST 612 loaded
	}
	if (id == '1')
	{
		displayError(1,false); //BCR-2G loaded
	}
	
	selectedSample_live = selectedSample_disc.substr(0, selectedSample_disc.indexOf('_holder'))+'.png';
	
	selectedSampleNum = num;
console.log ("selectedSampleNum ", selectedSampleNum)
console.log ("selectedSample_disc ", selectedSample_disc)
console.log ("sampleHolder ", sampleInHolder)
console.log ("id ", id);

	//if there's already a sample in that slot, remove and delete associated data, after confirm dialog
	
	if (selectedSample_disc.indexOf('empty')>0 )
	{
		loadingNewDisc = false;
		console.log ($("#sampleHolderImage"+	selectedSampleNum).attr('src'));
		if ($("#sampleHolderImage"+	selectedSampleNum).attr('src') && $("#sampleHolderImage"+	selectedSampleNum).attr('src').indexOf('empty')<0)
		{
			//removingDisc = true;
			removeDisc(id,num);
		}
	}
	else {
		loadingNewDisc = true;
	}
	console.log ("removing disc ", removingDisc)
	console.log ("loading new disc ", loadingNewDisc)
	
	
	if (loadingNewDisc)
	{
		
		//put name into sample name field
		$("#sampleName"+num).html(name);
		$("#sampleNameLaser"+num).html(name);
		$("#sampleHolderImage"+num).attr('src', selectedSample_disc);
		
		$("#sampleHolderImage"+num).attr('data-sample-name', name); //store the sample name as a custom attribute of the image
		
		$("#discHolder"+num).attr('src', selectedSample_disc); //also put in laser image 
		$("#discHolder"+num).attr ('data-sample-num',selectedSampleNum)
		$("#discHolder"+num).attr ('data-sample-name',name)
		$("#discHolder"+num).attr ('data-sample-id',id)
		
		
		$("#imgSampleDisc").attr('src', 'images/sample_disc_empty_holder.png');
		
		if (isLocalStorageSupported())
		{
			storage.setItem(("selectedSample"+num), selectedSample_disc);
			storage.setItem(("selectedSampleName"+num), name);
			storage.setItem(("selectedSampleId"+num), id);
			storage.setItem(("selectedSampleNum"), num);	
			storage.setItem("liveImageSrc", liveImageSrc);
		}
	}			
	
	$("#imgSampleDisc").attr('data-sample-name',"");	
	console.log (selectedSample_disc, removingDisc)
	
}

//just check user really wants to remove disc from sample holder
function removeDisc(id,num)
{
	$("#removeDiscDialog").dialog({
         title: "Remove disc?",
		 modal:true,
         buttons: {
             "Cancel": function() {
                 $(this).dialog('close');
				 removingDisc = false;
				 return;
             },
             "Continue": function() {
                $(this).dialog('close');
				removingDisc = true;
				//need to remove any ablation pits and placed spots associated with the sample we've just removed
				//these are tagged with a class specific to that sample
				$(".sample"+num).remove(); 
				$("#sampleName"+num).html("");
				$("#sampleHolderImage"+num).attr('data-sample-name', "");
				$("#sampleNameLaser"+num).html("");
				$("#sampleHolderImage"+num).attr('src', "");
				$("#imgSampleDisc").attr('src', 'images/sample_disc_empty_holder.png');
			
				$("#discHolder"+num).attr ('data-sample-num',selectedSampleNum)
				$("#discHolder"+num).attr ('data-sample-name',name)
				$("#discHolder"+num).attr ('data-sample-id',id)
				$("#discHolder"+num).attr ('src','images/sample_disc_empty_large.png')
				
				//check if the 2 standards are being removed and turn on errors if so
				if (id == '0')
				{
					displayError(0,true); //NIST 612 removed
				}
				if (id == '1')
				{
					displayError(1,true); //BCR-2G removed
				}
				
				//and also delete these rows from the scan list		
				for (var i=0; i<scanListTable.rows().count(); i++)
				{
					var dat = scanListTable.row( i ).data();
					
					if (dat[7] == num)
					{	
						scanListTable.$('tr').eq(i).addClass('toRemove'); //flag these rows for removal. If I remove them here then it shifts the indexes and screws things up
						storage.removeItem(("scanList")+dat[0]);					
					}
				}
				
				scanListTable.rows( '.toRemove' ).remove().draw(); //now we can remove all the rows associated with that sample
					
				for (var j = 1; j<=pitCount; j++)
				{
					if (storage.getItem("pit"+j))
					{
					var pitArray = storage.getItem("pit"+j).split(',');
					
					if (pitArray[4] == num)
						storage.removeItem("pit"+j);	
					}
				}
				
				storage.setItem(("selectedSample"+num), selectedSample_disc);
				storage.setItem(("selectedSampleName"+num), name);
				storage.setItem(("selectedSampleId"+num), id);
				storage.setItem(("selectedSampleNum"), num);	
				storage.setItem("liveImageSrc", liveImageSrc);
             }//end continue
         }
     });
	
}

/* one off button click at start of process, load and remove the error from message list. Bit tedious, could store like I do with some other controls
but Fran seems to want users to go through each of these steps */
function loadHolderInSampleCell(cb)
{
	samplesInCell = cb.checked;
	displayError(2,!samplesInCell);
	
	if (samplesInCell)
		$("#sampleHolder").css('border','solid #ffd200 4px'); //add a visual border so it looks like it's in the cell
	else
		$("#sampleHolder").css('border','solid #ffffff 4px'); 

}

//second stage, similar to above
function fillSampleCellWithHelium(cb)
{
	heliumOn = cb.checked;
	displayError(3,!heliumOn);
	
		
	if (heliumOn)
		$("#heliumIndicator").show();
	else
		$("#heliumIndicator").hide();
}


/*
-----------------------------------------Signal monitor tab---------------------------------------------------
*/

/* use High charts to make a dynamically updated graph of signal intensity (counts per second) for all selected elements.
will show background noise if laser not firing */
function plotCountGraph ()
{
	Highcharts.setOptions({
		time: {
			useUTC: false
		},	
	});

	liveGraph = new Highcharts.chart('graphContainer', {
    chart: {
        //type: 'spline',
        animation: Highcharts.svg, // don't animate in old IE
        marginRight: 5,
    },
	
	credits: {
        enabled: false
    },
    title: {
        text: ''
    },
    xAxis: {
        tickPixelInterval: 50,
		minRange:40,
		title: {text: 'time/ seconds'},
	
    },
    yAxis: {
		type: 'logarithmic', //n.b log scale
		tickInterval: 1,
		labels: {
                formatter: function () {
                    return Highcharts.numberFormat(this.value,0);
                }
            },
        title: {
            text: 'counts per second'
        },
		min:1,
		max:200000000,
        plotLines: [{
            value: 0,
            width: 1,
            color: '#808080'
        }]
    },
    tooltip: { 
		enabled: false 
	},
    legend: {
        enabled: true
    },
    exporting: {
        enabled: false
    }

	});
	
	//loop through all selected elements and add a series for it on the graph
	/*could do with improving the graph so starts at 0 but can't work out how to get it to append points and still keep a 40 second range on 
	x-axis unless the initial data has 40 points. Must be a way but don't really know what I'm doing with the API! */
	for (var i = 0; i < selectedElements.length; i++)
	{
		var atomicNum = selectedElements[i]
		var symbol = chemicalElements.elements[atomicNum-1].symbol;
		liveGraph.addSeries({
			data:[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
			
			marker:{enabled:false},
			name: symbol		
			});
	}
}

//update the signal monitor graph every second if timer is on 
function graphTimer(on)
{
	if (on)
	{
		if (!plasmaOn)
		{
			alert ("Plasma is off; you won't be able to analyse samples.");
			//return;
		}		
		graphInterval = setInterval(updateGraphCount, 1000); 	
	}
	else //stop timer
	{
		clearInterval(graphInterval);	
	}
}

//reset the signal monitor to zero
function resetMonitor()
{
	//graphTimer(false);
	plotCountGraph ();
}

//stop the signal monitor
function stopMonitor()
{
	graphTimer(false);
	$("#btnSignalMonitorStop").hide();
	$("#btnSignalMonitorStart").show();
}

//start the signal monitor
function startMonitor()
{
	graphTimer(true);
	$("#btnSignalMonitorStart").hide();
	$("#btnSignalMonitorStop").show();
}

//main drawing function for the signal intensity graph, called each second
function updateGraphCount ()
{
	//n.b. set 1st parameter to false to prevent redrawing of individual lines; better to redraw whole graph afterwards
	//second parameter is set to true to knock 1st point off series as new one is added at end, thus giving effect of scrolling graph 
	//rather than changing the scale and compressing the data points
	//the rangeSelector options available in highStock look useful but we don't have a licence for that? 

	//material = "sample";
	//console.log (material)
	if (!currentSampleId) currentSampleId = 0;
	var composition;
	if (material == "sample")
	{
		//array of concentrations for every element for the selected sample
		composition = data.samples[currentSampleId].composition; 
		//console.log ('using concentrations for sample id ', currentSampleId)
	}
	else if (material == "epoxy")
	{
		composition = data.epoxy;
//console.log ("using epoxy composition")		
	}
	
	graphTime ++;

	if (laserFiring) timeSinceLaserFired += 1000;
	if (washout) timeSinceLaserStopped += 1000;
	
	for (var i = 0; i < selectedElements.length; i++)
	{
		var atomicNum = selectedElements[i]
		
		/*
		this is just the published abundance of the element, as output in the data ouput table.
		we need to work back from this to simulate sensible signal intensities for each element
		also need to factor in effects of sliders on ICP-MS tab and reduce signal if they're not optimal. See below.
		*/
		var conc = composition[atomicNum-1];
		var conversionFactor = 1;
		//console.log ("conc of element " + atomicNum + " = " + conc);
		/*
		1st we need to cope with the fact that the major elements are in weight% oxides
		using Fran's conversion-factors.xlsx
		*/
		switch (atomicNum)
		{
			case 11: conversionFactor = 7419; break;//Na
			case 12: conversionFactor = 6031; break;//Mg
			case 13: conversionFactor = 5292; break;//Al
			case 14: conversionFactor = 4675; break;//Si
			case 19: conversionFactor = 8301; break;//K
			case 20: conversionFactor = 7147; break;//Ca
			case 26: conversionFactor = 6994; break;//Fe. This is for Fe3+. Fe2+ would be 7773
		}	
		var conc = conc * conversionFactor;
		
		/*
		now need to adjust for natural abundances of isotopes as we're only measuring one mass
		values in data file are currently for major isotope but this may not always be the one commonly measured
		in ICPMS as you pick other masses to avoid interference, e.g. use 43Ca instead of 40Ca to avoid 
		interference with 40Ar - the gas used in ionization. So I probably need Fran to provide a list of what
		isotopes she actually usually measures and put in abundances for those instead.
		*/
		var isotopeAbundance = data.isotopeAbundances[atomicNum - 1]

		conc = conc * (isotopeAbundance/100)
		//to do? maybe include values for 'ease of ionization' as these will affect signal counts
				
		var background = conc;
		
		/*
		attempt to 'compress' the range a bit by lifting up the lower values (but hardly changing the higher ones
		because for some reason on Fran's graph the less abundant elements have higher signals than I'd expect
		equation below is a best fit curve generated by mycurvefit.com where for x = 1, y = 50, x = 10, y = 5, x = 100, y = 2, x = 1000, y = 1
		*/
		var adjustment =  0.8685726 + (115.1314 - 0.8685726)/(1 + (background/0.7405977)^0.9387627)
		background = background * adjustment;
		
		var plusOrMinus = Math.random() < 0.5 ? -1 : 1;
		
		//trying to get the noise to be a higher proportion of signal at low intensities by having a fixed component as well as a conc-dependent part
		var randomNoise = (Math.random()*100)+(Math.random()*conc/5) * plusOrMinus;

		/*need a value to simulate downhole fractionation (as the laser ablates and the pit gets deeper
		then signal struggles to escape so counts drop) */
		if (!tuning)
			downholeFractionation = (1-timeSinceLaserFired/laserFiringDuration) + 0.8;
		else
			downholeFractionation = 1;
		
		//if we're firing over existing ablation pit reduce the signal intensity.
		if (firingOverExistingPit) 
			existingPitReductionFactor = 0.2;
		else
			existingPitReductionFactor = 1;
		
		if (washout) //once laser has stopped firing signal 'washes out' over a couple of seconds
		{
			downholeFractionation =  downholeFractionation * (1000/timeSinceLaserStopped - 0.2);

		}
		//so taking into account all the above modifiers to the basic concentration...
		var signalIntensity = conc * torchXVal * torchYVal * gasVal * downholeFractionation * existingPitReductionFactor;
	
		var scalingFactor = 1000; //just scaling to match counts in graph in Fran's spec as best I can
		signalIntensity = signalIntensity * scalingFactor;
		
		//we're now allowing them to fire laser even if plasma is off but they won't see any signal!
		if (!plasmaOn)
			signalIntensity = 0;
		
		signalIntensity = signalIntensity * spotSize/10 //reduce signal for small spot diameters
		signalIntensity = signalIntensity * repetitionRate/10; //and reduce for lower firing rates		
		var randomNoiseFiring = ((Math.random()*100)+(Math.random()*signalIntensity/5 ) + background/10)//* plusOrMinus; //more noise when firing, dependent on signal intensity
//console.log ("background ", background);
//console.log ("signal intensity ", signalIntensity);
//console.log ("randomNoiseFiring ", randomNoiseFiring);			
		if (laserFiring) {
			//plot a point for the concentration of each selected element according to the data file + a small random element
			
			//also make the noise much higher if over existing pit
			if (firingOverExistingPit) 
				randomNoiseFiring *=5;
//console.log(signalIntensity, randomNoiseFiring, downholeFractionation, firingOverExistingPit);
			/*last parameter in addPoint is 'shift' if false then point is appended and x axis rescaled;
			if true, as here, then axis shifts leftwards*/
			
			if (background + randomNoiseFiring  + signalIntensity <1)
				liveGraph.series[i].addPoint(1, false, true); //don't let it dip off the bottom of the graph when random noise more than signal + bg
			else
				liveGraph.series[i].addPoint(background + randomNoiseFiring  + signalIntensity, false, true);
//console.log ("point at ", background + randomNoiseFiring  + signalIntensity)			
		}
		else if (washout) //allow the signal to decay more gradually during a 'wash out' phase of a few secs
		{
			liveGraph.series[i].addPoint(background + randomNoiseFiring  + signalIntensity, false, true);
			
			if (timeSinceLaserStopped >=4000) //3 sec 'washout' period
			{
				washout = false;
				timeSinceLaserStopped = 0;
			}	
		}
		else {
			//just some low random noise to simulate background	
			liveGraph.series[i].addPoint( background + randomNoise,false, true);
			timeSinceLaserFired = 0;
		}
		
		if (!plasmaOn) //now allowing them to fire laser even if plasma is off, but then you won't see a signal
			liveGraph.series[i].addPoint( 0,false, true);
	}
	liveGraph.redraw()
}


/*
-----------------------------------------general functions---------------------------------------------------
*/

//checkbox for turning flashing effect on and off (in case of problems with epilepsy etc.)
function flashingCbHandler (cb)
{
	flashingOff = cb.checked;
	if (!laserFiring) return;
	if (!flashingOff) {
		$("#crossHairCircle").removeClass('laserLight');
		$("#crossHairCircle").addClass('laserLight'+repetitionRate); 
	}
	else {
		$("#crossHairCircle").removeClass('laserLight'+repetitionRate); 
		$("#crossHairCircle").addClass('laserLight'); //just a plain no-flashing laser!
	}
	
}

//each tab has a button for further information to appear in a pop-up. This info is in data file.
//thought this would be brief instructions but might just be a reference to the screen in 'Getting started' section
function showMoreInfo (tabNum)
{
	$( "#moreInfoDialog" ).dialog();
	
	switch (tabNum)
	{	
		case 11: $( "#moreInfoDialog" ).html(data.moreInfo[0]); break;
		case 12: $( "#moreInfoDialog" ).html (data.moreInfo[1]); break;
		case 13: $( "#moreInfoDialog" ).html (data.moreInfo[2]); break;
		case 21: $( "#moreInfoDialog" ).html (data.moreInfo[3]); break;
		case 22: $( "#moreInfoDialog" ).html (data.moreInfo[4]); break;
		case 23: $( "#moreInfoDialog" ).html (data.moreInfo[5]); break;
	}
	
	$("#moreInfoDialog").css('display','block');
}

//just returns the sample(disc) name in place in any of the 6 positions in the sample holder
function getSampleNameInPosition (posnum)
{
	if (posnum == 0) return 0;
	var sample = $("#discHolder"+posnum)[0].src;
	var start = sample.lastIndexOf('/')+1;
	var end = sample.lastIndexOf('.');
	sample = sample.substring(start,end);
	sample = sample.replace(/%20/g, " ");

	return sample;
}

//work out from coordinates of crosshairs which number in sample holder you're over
function getSampleHolderUnderCrosshair ()
{
	var holdernum = 0;
	if(isOverlap("#discHolder1","#crossHairCircle")) holdernum = 1;
	if(isOverlap("#discHolder2","#crossHairCircle")) holdernum = 2;
	if(isOverlap("#discHolder3","#crossHairCircle")) holdernum = 3;
	if(isOverlap("#discHolder4","#crossHairCircle")) holdernum = 4;
	if(isOverlap("#discHolder5","#crossHairCircle")) holdernum = 5;
	if(isOverlap("#discHolder6","#crossHairCircle")) holdernum = 6;
	
	return holdernum;
}

//generic function to check for overlap between two objects, e.g. when dragging sample over holder slot or detecting if over an existing ablation pit
function isOverlap(idOne,idTwo){
	
        var objOne=$(idOne),
            objTwo=$(idTwo),
            offsetOne = objOne.offset(),
            offsetTwo = objTwo.offset(),
            topOne=offsetOne.top,
            topTwo=offsetTwo.top,
            leftOne=offsetOne.left,
            leftTwo=offsetTwo.left,
            widthOne = objOne.width(),
            widthTwo = objTwo.width(),
            heightOne = objOne.height(),
            heightTwo = objTwo.height();
	
        var leftTop = leftTwo > leftOne && leftTwo < leftOne+widthOne && topTwo > topOne && topTwo < topOne+heightOne,             rightTop = leftTwo+widthTwo > leftOne && leftTwo+widthTwo < leftOne+widthOne && topTwo > topOne && topTwo < topOne+heightOne, leftBottom = leftTwo > leftOne && leftTwo < leftOne+widthOne && topTwo+heightTwo > topOne && topTwo+heightTwo < topOne+heightOne, rightBottom = leftTwo+widthTwo > leftOne && leftTwo+widthTwo < leftOne+widthOne && topTwo+heightTwo > topOne && topTwo+heightTwo < topOne+heightOne;
		
        return leftTop || rightTop || leftBottom || rightBottom;
}

//show or hide errors in messages window
function displayError(errornum, visible)
{
	if (!visible)
	{
		errorMessages[errornum] = "";
	}
	else
	{
		errorMessages[errornum] = data.errors[errornum];
	}
	
	$("#errorList").html(errorMessages);
}

//called when a spot is placed in live view. data added to table ready for batch analysis
function addToScanList(selectedSampleName, spotSize, repetitionRate,x,y,z, spotCount, overSampleNum,currentSampleId,material)
{
	//console.log (selectedSampleName, currentSampleId);
	/*add row. n.b only items that have an associated table header will be displayed but the rest are still stored
	as associated data */
	scanListTable.row.add( [
			spotCount,
            selectedSampleName,
            spotSize*5,
            repetitionRate,x,y,z,
			overSampleNum,
			currentSampleId,
			material
        ] ).draw( false );
	
	calculateBatchRunTime ();
	
	if (isLocalStorageSupported())
	{
		storage.setItem("scanList"+spotCount,
			[
				spotCount,
				selectedSampleName,
				spotSize,
				repetitionRate,x,y,z,
				overSampleNum,
				currentSampleId,
				material
			]);
		storage.setItem("spotCount",spotCount);
	}
}

//give users an estimate of how long the batch is going to take to complete
function calculateBatchRunTime ()
{	
	var batchRunTime = scanListTable.rows().count() *(laserFiringDuration + pauseBetweenAblations) / 60000; 
	$("#batchTimeDisplay").html ("(run will take " + batchRunTime + " mins)");
	if (batchRunTime > 0)
		$("#batchTimeDisplay").show();
	else
		$("#batchTimeDisplay").show();
}

/*
at the end of an ablation this populates the data output table with the abundances of the selected elements (up to 10)
These are taken straight from Fran's published MORB data (see MORB-data.csv). Note that the major elements are in weight% of oxides
whereas the trace elements are ppm. 
TO DO: 
- if we're over existing ablation pit then the output composition needs to reflect this
- and if we're over epoxy then output that composition, not the sample
- also need to cater for crystalline areas
*/
function addToDataTable (sampleNumber)
{
	var comp = ['','','','','','','','','',''];
//console.log(scanListSelectedData);
console.log(scanListSelectedData[9]);
	for (i = 0; i < selectedElements.length; i++) {
		var atomicNum = parseInt(selectedElements[i]);
		
		var abundance = data.samples[currentSampleId].composition[atomicNum-1];
		var m = scanListSelectedData[9]
		//TO DO: check what material this spot is placed on (sample, holder, epoxy) and adjust composition accordingly
		
		if (m == 'epoxy')
		{
			abundance = data.epoxy[atomicNum-1];
			console.log ("adjust abundances for epoxy " + abundance);
		}
		else if (m == 'sample holder') {
			abundance = data.sampleHolder[atomicNum-1];
		}
		/*check for firing over existing pit, either ones in the photo detected from the mask, m, though I think in practice we'll try to Photoshop these out to avoid confusion, or when over a pit that the user has created by previously ablating on that spot, i.e. firingOverExistingPit*/
		else if (m == 'an existing ablation pit'||firingOverExistingPit) 
		{
			
			console.log(m, firingOverExistingPit);
			/*do something here to adjust output composition to reflect that we're over the pit. Just increase variability - see random noise below - or use an alternative composition from the data file as we do with epoxy?*/
			//abundance = data.existingPit[atomicNum-1];
		}
		
		
		/*need to base randomNoise on abundance of element and for majors these are in wt% so we need to convert
		in order to distinguish between % and ppm */
		var conversionFactor = 1;
		switch (atomicNum)
		{
			case 11: conversionFactor = 7419; break;//Na
			case 12: conversionFactor = 6031; break;//Mg
			case 13: conversionFactor = 5292; break;//Al
			case 14: conversionFactor = 4675; break;//Si
			case 19: conversionFactor = 8301; break;//K
			case 20: conversionFactor = 7147; break;//Ca
			case 26: conversionFactor = 6994; break;//Fe. This is for Fe3+. Fe2+ would be 7773
		}	
		var conc = abundance * conversionFactor;
		
		var randomNoise = 10*Math.random()*(conc/1000); //simulate a little variation so 2 spots aren't exactly the same
		randomNoise = randomNoise/conversionFactor;
		
		/* 
		crude attempt to make this noise higher %age for low abundances (say 0.5% at higher intensities >10ppm, 10% at low <1ppm). Some function based on conc might be better
		*/
		if (conc<1)
			randomNoise *= 10;
		else if (conc >=1 && conc <5)
			randomNoise *= 5;
		else if (conc >=5 && conc <10)
			randomNoise *= 2;
		else if (conc >=10 && conc <100)
			randomNoise *= 1.5
		else if (conc >=100 && conc <1000)
			randomNoise *= 1.1;
		else if (conc >=1000 && conc <10000)
			randomNoise *= 1;
		else
			randomNoise *= 0.5
		
		/*See above. Adding extra variability if over an existing pit but may need to bias composition towards certain
		elements, in which case add per-element adjustment factors in data file */
		if (m == 'an existing ablation pit'||firingOverExistingPit) 
			randomNoise *= 10;
		
		randomNoise *= Math.floor(Math.random()*2) == 1 ? 1 : -1; // this will add minus sign in 50% of cases

		abundance =  Math.round ((randomNoise + abundance) * 1000) / 1000; //just rounding to 3 dec places for display
		
		if (abundance == 0) abundance = 'bdl'; //report as below detection limit
        comp[i] = abundance;
	}

	dataOutputTable.row.add( [
		data.samples[currentSampleId].title,
		comp[0],comp[1],comp[2],comp[3],comp[4],comp[5],comp[6],comp[7],comp[8],comp[9]

        ] ).draw( false );
	
	dataOutputSaved = false;
	
	/* To do? need to store contents of data table? how to deal with changes to elements selected?
	   would need to have columns for all elements present all the time
	   also need to provide way of exporting
	*/	
	
}

/*store settings in local storage (or potentially on VLE depending on where this is hosted?)*/
function storeSettings()
{
	if (!isLocalStorageSupported()) return;	
	if (!clearingAll) //don't want to store settings if we've just clicked the clearall btn!
	
	//which elements are selected on periodic table?
	storage.setItem("selectedElements", JSON.stringify(selectedElements));
	
	//sliders and checkboxes
	storage.setItem("torch1value", $('#slider_torch1').slider("option", "value"));
	storage.setItem("torch2value", $('#slider_torch1').slider("option", "value"));
	storage.setItem("gasvalue", $('#slider_gas').slider("option", "value"));
	
	storage.setItem("laserOn",$("#checkboxLaserOn").is(':checked'));
	storage.setItem("plasmaOn",$("#checkboxPlasma").is(':checked'));
	storage.setItem("flashingOff",$("#checkboxFlashing").is(':checked'));
	
	storage.setItem("spotSizeSelectedIndex",$("#selectSpotSize").val());
	storage.setItem("repetitionRateSelectedIndex",$("#selectRepetitionRate").val());	
	storage.setItem("laserMode", laserMode);
	
	storage.setItem("heliumOn",$("#checkboxHelium").is(':checked'));
	storage.setItem("samplesInCell",$("#checkboxLoadSampleCell").is(':checked'));
	
	//n.b. samples selected are stored in loadSample()
}

/*get settings from local storage (or potentially on VLE depending on where this is hosted?)*/
function retrieveSettings()
{
	//Think this is screwing up loading of tabs on iPad. Disabled by checking if it's a touch device until I can find a better solution!!
	//if (!isLocalStorageSupported() || hasTouch()) 
	if (!isLocalStorageSupported()) 
		return;

	
	//put in sample holder view
	$('#radio-2').click();
	
	//highlight selected elements
	if (storage.getItem("selectedElements"))
	{
		selectedElements = JSON.parse(storage.getItem("selectedElements"));
	
		for (var i = 0; i<selectedElements.length; i++)
		{
			$('#element_num_' + selectedElements[i]).addClass('elementSelected');
			var atomicNum = parseInt(selectedElements[i]);
			var symbol = chemicalElements.elements[atomicNum-1].symbol;
			var units = " (ppm)";
			//add oxides for major elements
			if (symbol == "Ca") { symbol = "CaO"; units = " (wt%)"; }
			if (symbol == "Mg") { symbol = "MgO"; units = " (wt%)"; }
			if (symbol == "Al") { symbol = "Al<sub>2</sub>O<sub>3</sub>"; units = " (wt%)"; }
			if (symbol == "K") { symbol = "K<sub>2</sub>O"; units = " (wt%)"; }
			if (symbol == "Na") { symbol = "Na<sub>2</sub>O"; units = " (wt%)"; }
			if (symbol == "Ti") { symbol = "TiO<sub>2</sub>"; units = " (wt%)"; }
			if (symbol == "Fe") { symbol = "Fe<sub>2</sub>O<sub>3</sub>"; units = " (wt%)"; }
			if (symbol == "Si") { symbol = "SiO<sub>2</sub>"; units = " (wt%)"; }
			 
			$("#th"+(i+1)).html(symbol + units);
	
		}
		if (selectedElements.length >0)
			displayError(7, false);
		//$("#btnSignalMonitorStart").removeClass('ui-state-disabled');
		plotCountGraph();
	}
	
	//values of sliders and checkboxes
	$("#slider_torch1").slider('value',storage.getItem("torch1value"));
	$("#slider_torch2").slider('value',storage.getItem("torch2value"));
	$("#slider_gas").slider('value',storage.getItem("gasvalue"));
	sliderTorchX (storage.getItem("torch1value"));
	sliderTorchY (storage.getItem("torch2value"));
	sliderGas (storage.getItem("gasvalue"));
	
	laserOn = (storage.getItem("laserOn") == 'true');
	displayError(4, !laserOn);
	$('#checkboxLaserOn').prop('checked', laserOn);
	$('#checkboxLaserOn').button( "refresh" );
	
	heliumOn = (storage.getItem("heliumOn") == 'true');
	displayError(3, !heliumOn);
	$('#checkboxHelium').prop('checked', heliumOn);
	$('#checkboxHelium').button( "refresh" );
	if (heliumOn) 
		$("#heliumIndicator").show();
	
	samplesInCell = (storage.getItem("samplesInCell") == 'true');
	displayError(2, !samplesInCell);
	$('#checkboxLoadSampleCell').prop('checked', samplesInCell);
	$('#checkboxLoadSampleCell').button( "refresh" );
	if (samplesInCell)
		$("#sampleHolder").css('border','solid #ffd200 4px'); 
	else
		$("#sampleHolder").css('border','solid #ffffff 4px'); 	
		
	plasmaOn = (storage.getItem("plasmaOn") == 'true');
	displayError(5, !plasmaOn);
	$('#checkboxPlasma').prop('checked', plasmaOn);
	$('#checkboxPlasma').button( "refresh" )
	
	flashingOff = (storage.getItem("flashingOff") == 'true');
	$('#checkboxFlashing').prop('checked', flashingOff);
	$('#checkboxFlashing').button( "refresh" )
		
	if (storage.getItem("spotSizeSelectedIndex"))
	{
		//console.log("spot size selected index = " + storage.getItem("spotSizeSelectedIndex"));
		$('#selectSpotSize').val(storage.getItem("spotSizeSelectedIndex"));
		$("#selectSpotSize").selectmenu("refresh");
		
		/*nb the following sets the spot size of all created spots by modifying .spotMarker class but what we really want is 
		for sizes to be set individually based on spotsize in scan list */
		
		setSpotSize (parseInt(storage.getItem("spotSizeSelectedIndex"))/5)
		//console.log (parseInt(storage.getItem("spotSizeSelectedIndex"))/5)
	}
	if (storage.getItem("repetitionRateSelectedIndex"))
	{
		$('#selectRepetitionRate').val(storage.getItem("repetitionRateSelectedIndex"));
		$("#selectRepetitionRate").selectmenu("refresh");
		setRepetitionRate (parseInt(storage.getItem("repetitionRateSelectedIndex")))
		//console.log (parseInt(storage.getItem("repetitionRateSelectedIndex")));
	}
	//any previously loaded samples

	selectedSampleNum = storage.getItem("selectedSampleNum");
	if (storage.getItem("liveImageSrc") != 'null')
		liveImageSrc = storage.getItem("liveImageSrc");

	for (var i = 1; i<=6; i++)
	{
		if (storage.getItem("selectedSample"+i))
		{	
			document.getElementById("sampleHolderImage"+i).src = storage.getItem("selectedSample"+i);
			$("#sampleHolderImage"+i).attr('data-sample-name', storage.getItem("selectedSampleName"+i));
			
			document.getElementById("discHolder"+i).src = storage.getItem("selectedSample"+i); 
			$("#discHolder"+i).attr('data-sample-name', storage.getItem("selectedSampleName"+i));
			
			$("#sampleHolderImage"+i).attr('data-sample-id', storage.getItem("selectedSampleId"+i));
			$("#discHolder"+i).attr('data-sample-id', storage.getItem("selectedSampleId"+i));	
			
			//restore sample names
			$("#sampleName"+i).html(storage.getItem("selectedSampleName"+i));
			$("#sampleNameLaser"+i).html(storage.getItem("selectedSampleName"+i));
			
			//turn off error messages about loading reference materials if they're already there
			if (storage.getItem("selectedSampleId"+i) == '0')
				displayError(0,false)
			if (storage.getItem("selectedSampleId"+i) == '1')
				displayError(1,false)
		}
	}
	
	//and placed spots
	if (!isNaN(storage.getItem("spotCount")))
		spotCount = storage.getItem("spotCount");
		
	for (var i = 1; i<=spotCount; i++)
	{
		if (storage.getItem("scanList"+i))
		{		
			var spotArray = storage.getItem("scanList"+i).split(',');

			addToScanList(spotArray[1],spotArray[2],spotArray[3],spotArray[4],spotArray[5],spotArray[6],spotArray[0], spotArray[7], spotArray[8], spotArray[9]);
			
			//place visual indicator within live image
			var spot = "<div id = 'spot"+i +"' class = 'spotMarker'></div";
			var imLeft = parseInt(spotArray[4]);
			var imTop = parseInt(spotArray[5]);
			if (isNaN(imLeft)) imLeft = 0;
			if (isNaN(imTop)) imTop = 0;
			var ss = parseInt(spotArray[2]);
			var zl = parseInt(spotArray[6]);

			var l = (159 - imLeft - zl*ss/2)/zl;
			var t = (159 - imTop  - zl*ss/2)/zl;
			
			$("#liveImageDiv" ).append( spot);
			$("#spot"+i).css('left',l+'px').css('top',t+'px');
			$("#spot"+i).css('position','absolute').css('width',ss+'px').css('height', ss+'px');
			
			$("#spot"+i).addClass ('sample'+spotArray[7]); //add a class I can use to track which spot is on which sample		
		}
	}
	
	//and ablation pits	
	if (!isNaN(storage.getItem("pitCount")))
		pitCount = storage.getItem("pitCount");
		
	for (var i = 1; i<=pitCount; i++)
	{
		if (storage.getItem("pit"+i))
		{		
			var pitArray = storage.getItem("pit"+i).split(',');

			//place visual indicator within live image
			var im = document.getElementById('liveImageDiv');
			var pit = "<div id = 'pit"+i +"' class = 'ablationPit'></div";
			var imLeft = parseInt(pitArray[0]);
			var imTop = parseInt(pitArray[1]);
			if (isNaN(imLeft)) imLeft = 0;
			if (isNaN(imTop)) imTop = 0;
			var ss = parseInt(pitArray[2]);
			var zl = parseInt(pitArray[3]);
		
			var l = (159 - imLeft - zl*ss/2)/zl;
			var t = (159 - imTop  - zl*ss/2)/zl;
			
			$( "#liveImageDiv" ).append( pit);
			$("#pit"+i).css('left',l+'px').css('top',t+'px');
			$("#pit"+i).css('position','absolute').css('width',ss+'px').css('height',ss+'px');
			
			$("#pit"+i).addClass ('sample'+pitArray[4]); //add a class I can use to track which pit is on which sample
		}
	}
} //end retrieveSettings

//check before trying to use local storage. Doesn't work on local machine in IE. 
function isLocalStorageSupported() {
  var testKey = 'test', storage = window.localStorage;
  try {
    storage.setItem(testKey, '1');
    storage.removeItem(testKey);
    return true;
  } catch (error) {
    return false;
  }
}

//clears local storage and reloads
function clearAll()
{
	$("#clearAllWarningDialog").dialog({
         title: "Clear all settings?",
		 modal:true,
         buttons: {
             "Cancel": function() {
                 $(this).dialog('close');
				 return;
             },
             "Continue": function() {
                $(this).dialog('close');
				
				//browser remembers checkboxes so need to clear these manually to fully reset
				$('#checkboxLaserOn').prop('checked', false);
				$('#checkboxLaserOn').button( "refresh" );
				$('#checkboxFireLaser').prop('checked', false);
				$('#checkboxFireLaser').button( "refresh" );
				$('#checkboxPlasmaOn').prop('checked', false);
				$('#checkboxPlasmaOn').button( "refresh" );
				$('#checkboxLoadSampleCell').prop('checked', false);
				$('#checkboxLoadSampleCell').button( "refresh" );
				$('#checkboxHelium').prop('checked', false);
				$('#checkboxHelium').button( "refresh" );
				$('#slider_torch1').slider('value', 0);
				$('#slider_torch2').slider('value', 0);
				$('#slider_gas').slider('value', 0);
				clearingAll = true;
				storage.clear();
				location.reload();	
             }
         }
     });
	
}

/*mini slideshows within teaching material, with accompanying audio if present*/
function createSlideshow (s, n, dat)
{
	numberOfSlides = n;
	slideshowNum = s;
	slideNum = 1;
	slidedata[slideshowNum-1] = dat;
console.log("createSlideshow ", n,s,slidedata)
	$("#slideshow" + slideshowNum).attr ('src', slidedata[slideshowNum-1].slides[0].fileName);
	$("#slideshow" + slideshowNum+"caption").html (slidedata[slideshowNum-1].slides[0].caption);
}

function previousSlide()
{	
	slideNum --;
	
	if (slideNum == 0) slideNum = numberOfSlides;
	$("#slideshow" + slideshowNum).attr ('src', slidedata[slideshowNum-1].slides[slideNum-1].fileName);
	$("#slideshow" + slideshowNum+"caption").html (slidedata[slideshowNum-1].slides[slideNum-1].caption);
	if (slidedata[slideshowNum-1].slides[slideNum-1].audio != 'undefined')
		updateAudioSource (slideNum);
}

function nextSlide()
{
	slideNum ++;
	console.log (slidedata[slideshowNum-1]);
	if (slideNum > numberOfSlides) slideNum = 1;
	$("#slideshow" + slideshowNum).attr ('src', slidedata[slideshowNum-1].slides[slideNum-1].fileName);
	$("#slideshow" + slideshowNum+"caption").html (slidedata[slideshowNum-1].slides[slideNum-1].caption);
	if (slidedata[slideshowNum-1].slides[slideNum-1].audio != '')
		updateAudioSource (slideNum);
}

function updateAudioSource(){ 
    var audio = document.getElementById('slideshowAudio');
    audio.src = 
        'audio/' + slidedata[slideshowNum-1].slides[slideNum-1].audio
    audio.load();
	
	$("#slideshow"+slideshowNum+"_transcript").html(slidedata[slideshowNum-1].slides[slideNum-1].transcript);
    
}

//download Excel file of values for reference standards
function downloadRefValues ()
{
	//location.href = 'data/ref_values.xlsx';
	window.open('data/ref_values.xlsx');
}

function transcript (transcript_div)
{
	$("#"+transcript_div).toggle();
}

/* returns an RGBA value for an XY coordinate, with a view to using it to discriminate between
sample glass and background epoxy
n.b. doesn't work on local machine as latest browsers don't like tainted canvas and give security error
called when placing spots. Could also check this when firing laser manually
*/
function getPixelColor(x,y)
{
	
	var imgMap=document.getElementById("liveImageOffscreen");
	
	var canvas = document.createElement('canvas');
	
	canvas.width = imgMap.width;
	canvas.height = imgMap.height;
	ctx = canvas.getContext('2d');
	
	ctx.drawImage(imgMap, 0, 0, imgMap.width, imgMap.height);
	
	//how many pixels to average colour data over, rather than just relying on one pixel, which could be a rogue reflection or something
	//or link to spotSize since what you're interested in is what's in the circle the laser will ablate?
	var pointSize = 1; //1 is probably OK as long as we're using offscreen bitmap with solid colour areas 
	var pixelData;
	try {
		pixelData = ctx.getImageData(x,y, pointSize, pointSize).data;

		return pixelData;
	}
		catch(error) {
			//console.error(error);
			//running locally so can't get value as operation is insecure
			return [100,100,100,255];
		}


	
}

/*
to do. use the rgb value returned above to discriminate between sample and background epoxy (light). 
using offscreen image maps instead as there's too much 'fuzziness' in real samples. These are coloured so
red is epoxy, black is existing ablation pits (if we haven't Photoshopped them out and so anything else is assumed
to be sample glass
*/
function sampleOrEpoxy(x,y)
{
	
	var rgba = getPixelColor(x,y)
	console.log('R: ' + rgba[0] + '<br>G: ' + rgba[1] + '<br>B: ' + rgba[2] + '<br>A: ' + rgba[3]);
	
	if (rgba[3] < 255) //on transparent bit of png, i.e. outside circle
		material = 'sample holder';

	else if (rgba[0] > 250)
		material = 'epoxy'
	else if (rgba[0] <10) //black
		material = 'an existing ablation pit'
		
	else //assume it's on the glass
		material = 'sample'
		
	console.log (material, batchRunning);
	
	if (material != 'sample' && !batchRunning) //alert if spot not on sample but don't interrupt batch run (it'll just give little or no signal)
		alert ('It looks like your spot might be on ' + material + ', not on the volcanic glass')
}






